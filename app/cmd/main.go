package main

import (
	"context"
	"gitlab.com/konfka/docker-compose/app/repository"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/konfka/docker-compose/app/internal/service"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/konfka/docker-compose/app/internal/controller"
)

func main() {

	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	repo := repository.NewVacancyStorage()
	parser := controller.NewVacancyPars()
	VacancyService := service.NewUserService(repo, parser)
	VacancyController := controller.NewVacancyController(VacancyService)

	r.Post("/search", VacancyController.Search)
	r.Delete("/vacancy/{id}", VacancyController.Delete)
	r.Get("/vacancies", VacancyController.GetList)
	r.Get("/vacancy/{id}", VacancyController.GetByID)

	//SwaggerUI
	r.Get("/swagger", controller.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./app/internal/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Printf("server started on port %s ", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
