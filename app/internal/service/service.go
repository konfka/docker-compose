package service

import (
	"gitlab.com/konfka/docker-compose/app/internal/entity"
	"log"
)

type VacancyParser interface {
	Parse(str string) ([]entity.Vacancy, error)
}

type VacancyService interface {
	Create(entity.Vacancy) error
	GetByID(id int) (entity.Vacancy, error)
	GetList() ([]entity.Vacancy, error)
	Delete(id int) error
}

type UserService struct {
	repo VacancyService
	pars VacancyParser
}

func NewUserService(service VacancyService, parser VacancyParser) *UserService {
	return &UserService{repo: service, pars: parser}
}

func (u *UserService) Search(query string) {
	vacancies, err := u.pars.Parse(query)
	if err != nil {
		log.Fatalln(err)
		return
	}

	for i := range vacancies {
		err = u.repo.Create(vacancies[i])
		if err != nil {
			log.Fatalln(err)
			return
		}
	}
	log.Println("End of parsing")
}

func (u *UserService) Create(dto entity.Vacancy) error {
	return u.repo.Create(dto)
}

func (u *UserService) GetByID(id int) (entity.Vacancy, error) {
	return u.repo.GetByID(id)
}

func (u *UserService) GetList() ([]entity.Vacancy, error) {
	return u.repo.GetList()
}

func (u *UserService) Delete(id int) error {
	return u.repo.Delete(id)
}
