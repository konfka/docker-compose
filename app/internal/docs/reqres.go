package docs

import (
	"gitlab.com/konfka/docker-compose/app/internal/entity"
)

//go:generate swagger generate spec -o /Users/svecsofa/go/src/gitlab.com/konfka/docker-compose/app/internal/public/swagger.json --scan-models

// swagger:route POST /search vacancy vacancySearchRequest
// Поиск вакансий.
// responses:
//   200: vacancySearchResponse

// swagger:parameters vacancySearchRequest
type vacancySearchRequest struct {
	// Search bat
	// in: formData
	Query string `json:"query"`
}

// swagger:route DELETE /vacancy/{id} vacancy vacancyDeleteRequest
// Удаление вакансии по ID.
// responses:
//   200: vacancyDeleteRequest

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// in:body
	ID string `json:"id"`
}

// swagger:route GET /vacancy/{id} vacancyGetByIDRequest
// Поиск вакансии по ID.
// responses:
//   200: vacancyGetByIDRequest
//	 400: description: invalid id supplied
//   500: description: invalid writing response

// swagger:parameters vacancyGetByIDRequest
type vacancyGetByIDRequest struct {
	// in:body
	ID string `json:"id"`
}

// swagger:parameters vacancyGetByIDResponse
type vacancyGetByIDResponse struct {
	// in:body
	Body entity.Vacancy
}

// swagger:route GET /vacancies vacancyGetListRequest
// Показать список всех вакансий
// responses:
//   200: vacancyGetByIDRequest
//	 400: description: invalid id supplied
//   500: description: invalid writing response

// swagger:parameters vacancyGetListRequest
type vacancyGetListRequest struct {
}

// swagger:parameters vacancyGetByIDResponse
type vacancyGetListResponse struct {
	// in:body
	Body []entity.Vacancy
}
